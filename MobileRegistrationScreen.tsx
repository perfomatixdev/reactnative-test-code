import React, { useContext, useState } from 'react';
import { Image, Keyboard } from 'react-native';

import { AppContext } from '@context';
import { ASYNC_STORE, storeData } from '@storage';
import {
  Button,
  ContentWrapper,
  EntryTitleView,
  FooterBar,
  HeaderBar,
  Modal,
  RowBar,
  SelectInput,
  Text,
  TextInput
} from '@components';
import { CancelRegistrationView, CountrySelectionView } from '@screens';
import { EntryLayout } from '@layout';
import { I18n, validate } from '@lib';

import { useAxiosPost, useBackHandler } from '@hook';

import { closeIcon, flag, greyDropDownIcon } from '@assets';

import { getAPI } from '@constants';

import NumberVerificationView from './NumberVerificationView';

const MobileRegistrationScreen: React.FC<any> = ({ navigation }: any) => {
  const [country, setCountry] = useState({
    countryCode: '+1',
    icon: flag,
    title: 'USA'
  });
  const [isVisible, setIsVisible] = useState({
    cancelRegistration: false,
    numberVerification: false
  });
  const [focus, setFocus] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [validationError, setValidationError] = useState({ phoneNumber: '' });
  const { context: { userDetails }, setContext } = useContext<any>(AppContext);

  useBackHandler(() => setIsVisible({
    ...isVisible,
    cancelRegistration: true
  }));

  const handleError = (): void => {
    if (isVisible.numberVerification === false) {
      setIsVisible({
        ...isVisible,
        numberVerification: true
      });
    }
  };

  const handleSuccess = (): void => {
    setContext({
      userDetails: {
        ...userDetails,
        countryCode: country.countryCode,
        phoneNumber
      }
    });
    storeData(ASYNC_STORE.LOGIN_DETAILS.COUNTRY_CODE, country.countryCode);
    storeData(ASYNC_STORE.LOGIN_DETAILS.PHONE_NUMBER, phoneNumber);

    navigation.navigate('OtpVerificationScreen');
  };

  const onBlur = (): void => {
    setFocus(false);
  };

  const onFocus = (): void => {
    setFocus(true);
    setValidationError({
      ...validationError,
      phoneNumber: ''
    });
  };

  const [
    { error },
    phoneNumberValidation
  ] = useAxiosPost(getAPI.PHONE_VALIDATION, {
    onError: () => handleError(),
    onSuccess: () => handleSuccess()
  });

  navigation.setOptions({
    header: (props: any) => (
      <HeaderBar
        color="light"
        leftIcon={closeIcon}
        leftIconPress={() => setIsVisible({
          ...isVisible,
          cancelRegistration: !isVisible.cancelRegistration
        })}
        {...props}
      />
    )
  });

  const handleCodeChange = ((value: any): void => {
    setCountry(value);
  });

  function onPressSend(): void {
    Keyboard.dismiss();

    const validateData = {
      phoneNumber: { type: 'number', value: phoneNumber }
    };
    setValidationError(validate(validateData));
    if (Object.keys(validate(validateData)).length === 0) {
      const apiPostData = {
        data: {
          phone_code: country.countryCode,
          phone_number: phoneNumber,
          profile_uid: userDetails.profile_uid
        }
      };
      phoneNumberValidation(apiPostData);
    }
  }

  return (
    <>
      <EntryLayout>
        <EntryTitleView
          subTitle={I18n.t('screen.mobile_registration_screen.sub_title')}
          title={I18n.t('screen.mobile_registration_screen.title')}
        />
        <ContentWrapper
          gutterX="none"
          gutterY="wide"
        >
          <RowBar
            withBottomGap={false}
          >
            <SelectInput
              value={country.icon}
              variant="country"
              onValueChange={handleCodeChange}
              onSelection={
                (countryPickerisVisible: boolean,
                  handleSelect: Function,
                  dismissModal: Function) => (
                    <Modal
                      isVisible={countryPickerisVisible}
                      screen={(
                        <CountrySelectionView
                          onClose={dismissModal}
                          onPressListItem={handleSelect}
                          value={country.countryCode}
                        />
                      )}
                    />
                  )
              }
            />
            <ContentWrapper
              gutterRight="small"
              gutterX="none"
              gutterY="none"
            >
              <Image
                resizeMode="contain"
                source={greyDropDownIcon}
              />
            </ContentWrapper>
            <TextInput
              color={validationError.phoneNumber ? 'negative' : 'accent'}
              countryCode={country.countryCode}
              focus={focus}
              keyboardType="numeric"
              maxLength={country.countryCode === '+41' ? 9 : 10}
              onChangeText={setPhoneNumber}
              onBlur={() => onBlur()}
              onFocus={() => onFocus()}
              value={phoneNumber}
              variant="phone"
            />
          </RowBar>
          <ContentWrapper
            gutterLeft="wide"
            gutterX="hairLine"
            gutterY="hairLine"
          >
            <ContentWrapper
              gutterLeft="medium"
              gutterX="none"
              gutterY="none"
            >
              <Text
                color="primary"
                size="xxsmall"
              >
                {I18n.t('screen.mobile_registration_screen.phone_number_helper_text_1')}
                <Text
                  color="accent"
                  size="xxsmall"
                >
                  {I18n.t('screen.mobile_registration_screen.phone_number_helper_text_2')}
                </Text>
              </Text>
            </ContentWrapper>
          </ContentWrapper>
          <ContentWrapper
            gutterX="none"
            gutterY="wide"
          >
            <Button
              disabled={phoneNumber.length < 9}
              onPress={onPressSend}
              title={I18n.t('button.send')}
            />
          </ContentWrapper>
        </ContentWrapper>
        <></>
        <FooterBar />
      </EntryLayout>
      <Modal
        isVisible={isVisible.cancelRegistration}
        onBackButtonPress={() => setIsVisible({
          ...isVisible,
          cancelRegistration: false
        })}
        screen={(
          <CancelRegistrationView
            buttonLabelOne={I18n.t('button.register')}
            buttonLabelTwo={I18n.t('button.quit')}
            helperTextOne={I18n.t('screen.mobile_registration.cancel_registration_view.helper_text_one')}
            helperTextTwo={I18n.t('screen.mobile_registration.cancel_registration_view.helper_text_two')}
            onClose={() => setIsVisible({
              ...isVisible,
              cancelRegistration: !isVisible.cancelRegistration
            })}
            subTitle={I18n.t('screen.mobile_registration.cancel_registration_view.sub_title')}
            title={I18n.t('screen.mobile_registration.cancel_registration_view.title')}
          />
        )}
      />
      <Modal
        isVisible={isVisible.numberVerification}
        onBackButtonPress={() => setIsVisible({
          ...isVisible,
          numberVerification: false
        })}
        screen={(
          <NumberVerificationView
            existingNumber={error?.response?.status === 401}
            onClose={() => setIsVisible({
              ...isVisible,
              numberVerification: !isVisible.numberVerification
            })}
          />
        )}
      />
    </>
  );
};

export default MobileRegistrationScreen;
