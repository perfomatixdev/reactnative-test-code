import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { BaseLayout } from '@layout';
import { ContentWrapper, ScreenWrapper } from '@components';
import { theme } from '@styles';

interface FormLayout {
  children: React.ReactNode;
}

const styles: any = StyleSheet.create({
  headerSection: {
    borderBottomWidth: 1,
    borderColor: theme.colors.grey_light,
    flex: 1
  },
  contentWrapper: {
    flex: 2
  }
});

const FormLayout: React.FC<FormLayout> = ({ children }) => {
  const childArray = React.Children.toArray(children);

  return (
    <BaseLayout>
      <ScreenWrapper backgroundColor="light">
        <View style={styles.headerSection}>
          <ContentWrapper
            fill
            gutterX="wide"
            gutterY="large"
            justify="center"
          >
            {childArray[0]}
          </ContentWrapper>
        </View>
        <View style={styles.contentWrapper}>
          <ContentWrapper
            fill
            gutterX="none"
            gutterY="small"
            justify="center"
          >
            <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
              {childArray[1]}
            </KeyboardAwareScrollView>
            <ContentWrapper
              fill
              gutterX="wide"
              gutterY="large"
              justify="bottom"
            >
              {childArray[2]}
            </ContentWrapper>
          </ContentWrapper>
          {childArray[3]}
        </View>
      </ScreenWrapper>
    </BaseLayout>
  );
};

FormLayout.propTypes = {
  children: PropTypes.node
};

FormLayout.defaultProps = {
  children: null
};

export default FormLayout;
