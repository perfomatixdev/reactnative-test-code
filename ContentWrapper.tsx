import PropTypes from 'prop-types';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import normalize from 'react-native-normalize';

interface ContentWrapperProps {
  align?: string | null;
  children: React.ReactNode;
  fill?: boolean;
  gutterBottom?: string;
  gutterLeft?: string;
  gutterRight?: string;
  gutterTop?: string;
  gutterX?: string;
  gutterY?: string;
  justify?: string | null;
}

const styles: any = StyleSheet.create({
  // Align
  contentWrapper_centerAlign: {
    alignItems: 'center'
  },
  contentWrapper_leftAlign: {
    alignItems: 'flex-start'
  },
  contentWrapper_rightAlign: {
    alignItems: 'flex-end'
  },

  // Justify
  contentWrapper_centerJustify: {
    justifyContent: 'center'
  },
  contentWrapper_topJustify: {
    justifyContent: 'flex-start'
  },
  contentWrapper_bottomJustify: {
    justifyContent: 'flex-end'
  },
  contentWrapper_spaceBetweenJustify: {
    justifyContent: 'space-between'
  },

  // Gutter
  contentWrapper_hairLineBottom: {
    marginBottom: normalize(4, 'height')
  },
  contentWrapper_hairLineLeft: {
    marginLeft: normalize(4)
  },
  contentWrapper_hairLineRight: {
    marginRight: normalize(4)
  },
  contentWrapper_hairLineTop: {
    marginTop: normalize(4, 'height')
  },
  contentWrapper_hairLineX: {
    marginHorizontal: normalize(4)
  },
  contentWrapper_hairLineY: {
    marginVertical: normalize(4, 'height')
  },
  contentWrapper_smallBottom: {
    marginBottom: normalize(8, 'height')
  },
  contentWrapper_smallLeft: {
    marginLeft: normalize(8)
  },
  contentWrapper_smallRight: {
    marginRight: normalize(8)
  },
  contentWrapper_smallTop: {
    marginTop: normalize(8, 'height')
  },
  contentWrapper_smallX: {
    marginHorizontal: normalize(8)
  },
  contentWrapper_smallY: {
    marginVertical: normalize(8, 'height')
  },
  contentWrapper_mediumBottom: {
    marginBottom: normalize(16, 'height')
  },
  contentWrapper_mediumLeft: {
    marginLeft: normalize(16)
  },
  contentWrapper_mediumRight: {
    marginRight: normalize(16)
  },
  contentWrapper_mediumTop: {
    marginTop: normalize(16, 'height')
  },
  contentWrapper_mediumX: {
    marginHorizontal: normalize(16)
  },
  contentWrapper_mediumY: {
    marginVertical: normalize(16, 'height')
  },
  contentWrapper_largeBottom: {
    marginBottom: normalize(24, 'height')
  },
  contentWrapper_largeLeft: {
    marginLeft: normalize(24)
  },
  contentWrapper_largeRight: {
    marginRight: normalize(24)
  },
  contentWrapper_largeTop: {
    marginTop: normalize(24, 'height')
  },
  contentWrapper_largeX: {
    marginHorizontal: normalize(24)
  },
  contentWrapper_largeY: {
    marginVertical: normalize(24, 'height')
  },
  contentWrapper_xlargeBottom: {
    marginBottom: normalize(28, 'height')
  },
  contentWrapper_xlargeLeft: {
    marginLeft: normalize(28)
  },
  contentWrapper_xlargeRight: {
    marginRight: normalize(28)
  },
  contentWrapper_xlargeTop: {
    marginTop: normalize(28, 'height')
  },
  contentWrapper_xlargeX: {
    marginHorizontal: normalize(28)
  },
  contentWrapper_xlargeY: {
    marginVertical: normalize(28, 'height')
  },
  contentWrapper_wideBottom: {
    marginBottom: normalize(32, 'height')
  },
  contentWrapper_wideLeft: {
    marginLeft: normalize(32)
  },
  contentWrapper_wideRight: {
    marginRight: normalize(32)
  },
  contentWrapper_wideTop: {
    marginTop: normalize(32, 'height')
  },
  contentWrapper_wideX: {
    marginHorizontal: normalize(32)
  },
  contentWrapper_wideY: {
    marginVertical: normalize(32, 'height')
  },
  // Fill
  contentWrapperFill: {
    flex: 1
  }
});
const ContentWrapper: React.FC<ContentWrapperProps> = ({
  align,
  children,
  fill,
  gutterBottom,
  gutterLeft,
  gutterRight,
  gutterTop,
  gutterX,
  gutterY,
  justify
}) => (
  <View
    style={[
      align && styles[`contentWrapper_${align}Align`],
      gutterBottom && styles[`contentWrapper_${gutterBottom}Bottom`],
      gutterLeft && styles[`contentWrapper_${gutterLeft}Left`],
      gutterRight && styles[`contentWrapper_${gutterRight}Right`],
      gutterTop && styles[`contentWrapper_${gutterTop}Top`],
      gutterX && styles[`contentWrapper_${gutterX}X`],
      gutterY && styles[`contentWrapper_${gutterY}Y`],
      justify && styles[`contentWrapper_${justify}Justify`],
      fill && styles.contentWrapperFill
    ]}
  >
    {children}
  </View>
);

ContentWrapper.propTypes = {
  align: PropTypes.oneOf(['center', 'left', 'right']),
  children: PropTypes.node,
  fill: PropTypes.bool,
  gutterBottom: PropTypes.oneOf(['hairLine', 'small', 'medium', 'none', 'large', 'xlarge', 'wide']),
  gutterLeft: PropTypes.oneOf(['hairLine', 'small', 'medium', 'none', 'large', 'xlarge', 'wide']),
  gutterRight: PropTypes.oneOf(['hairLine', 'small', 'medium', 'none', 'large', 'xlarge', 'wide']),
  gutterTop: PropTypes.oneOf(['hairLine', 'small', 'medium', 'none', 'large', 'xlarge', 'wide']),
  gutterX: PropTypes.oneOf(['hairLine', 'small', 'medium', 'none', 'large', 'xlarge', 'wide']),
  gutterY: PropTypes.oneOf(['hairLine', 'small', 'medium', 'none', 'large', 'xlarge', 'wide']),
  justify: PropTypes.oneOf(['center', 'top', 'bottom', 'spaceBetween'])
};

ContentWrapper.defaultProps = {
  align: null,
  children: null,
  fill: false,
  gutterBottom: 'none',
  gutterLeft: 'none',
  gutterRight: 'none',
  gutterTop: 'none',
  gutterX: 'medium',
  gutterY: 'medium',
  justify: null
};

export default ContentWrapper;
