import PropTypes from 'prop-types';
import React from 'react';
import { ActivityIndicator, Image, StyleSheet, TouchableOpacity, ViewPropTypes } from 'react-native';
import normalize from 'react-native-normalize';

import {
  getButtonBorderColor,
  getButtonDisabledColor,
  getButtonTextColor,
  theme
} from '@styles';
import { Text } from '@components';

interface ButtonProps {
  color?: string | null;
  disabled?: boolean | null;
  icon?: any|null;
  loading?: boolean|null;
  size?: string | null;
  style?: object | null;
  subColor?: string | null;
  title: string | null;
  variant?: string | null;
}

const styles: any = StyleSheet.create({
  // Wrapper
  buttonWrapper: {
    height: normalize(40, 'height'),
    justifyContent: 'center',
    alignItems: 'center'
  },
  button_normalWrapper: {
    width: normalize(148)
  },
  button_largeWrapper: {
    width: normalize(174)
  },
  button_wideWrapper: {
    width: normalize(200)
  },

  // Colors
  ...Object.keys(theme.colors).reduce((colorStyles: object, color: string) => ({
    ...colorStyles,
    [`button_${color}Color`]: {
      backgroundColor: theme.colors[color],
      borderColor: theme.colors[getButtonBorderColor(color)]

    },
    [`button_${color}Border`]: {
      borderColor: theme.colors[color]
    }
  }), {}),

  // Variant
  button_rounded: {
    borderRadius: normalize(20, 'height'),
    borderWidth: 1,
    borderStyle: 'solid'
  }
});

const Button: React.FC<ButtonProps> = ({
  color,
  disabled,
  icon,
  loading,
  size,
  style,
  subColor,
  title,
  variant,
  ...other
}) => {
  let content = null;

  if (loading) {
    content = <ActivityIndicator animating={loading} size="small" color={theme.colors.light} />;
  } else if (icon) {
    content = (
      <Image
        source={icon}
        resizeMode="contain"
      />
    );
  } else {
    content = (
      <Text
        color={subColor || getButtonTextColor(color)}
        fontFamily="secondary"
        size="medium"
        weight="bold"
      >
        {title}
      </Text>
    );
  }

  return (
    <TouchableOpacity
      style={[
        styles.buttonWrapper,
        styles[`button_${size}Wrapper`],
        styles[`button_${variant}`],
        disabled ? styles[`button_${getButtonDisabledColor(color)}Color`] : styles[`button_${color}Color`],
        subColor && styles[`button_${subColor}Border`],
        !!style && style
      ]}
      disabled={disabled}
      {...other}
    >

      {content}
    </TouchableOpacity>
  );
};

Button.propTypes = {
  color: PropTypes.oneOf(Object.keys(theme.colors)),
  disabled: PropTypes.bool,
  icon: PropTypes.any,
  loading: PropTypes.bool,
  size: PropTypes.oneOf(['normal', 'large', 'wide']),
  style: ViewPropTypes.style,
  subColor: PropTypes.oneOf(Object.keys(theme.colors)),
  title: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['rounded'])
};

Button.defaultProps = {
  color: 'primary',
  disabled: false,
  icon: null,
  loading: false,
  size: 'normal',
  style: null,
  subColor: null,
  variant: 'rounded'
};

export default Button;
