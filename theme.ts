const theme: any = {
  colors: {
    primary: '#FFFFFF',
    accent: '#000000',

    primary_alpha: 'rgba(140,98,219,0.7)',
    primary_light: '#9E84DC',
    primary_dark: '#6B51A9',
    primary_darker: '#8961D4',
    primary_lighter: '#B6A3E5',

    light: '#ffffff',
    grey_light: '#EAEAEA',
    grey_lighter: '#FAFAFA',
    grey: '#9B9B9B',
    grey_dark: '#646464',
    positive: '#88C041',
    neutral: '#f5f5f5',
    negative: '#E14C4C',
    negative_light: '#FF6A70',
    negative_light_alpha: 'rgba(255,106,112,0.7)',

    transparent: 'transparent',
    transparent_light: 'rgba(0,0,0,0.04)'
  },
  fontFamily: {
    primary: 'Font1',
    secondary: 'Font2'
  }
};

export function getButtonTextColor(colorKey: string): string {
  switch (colorKey) {
    case 'primary':
    case 'primary_lighter':
    case 'negative_light':
      return 'light';
    case 'light':
      return 'primary';
    default:
      return colorKey;
  }
}

export function getButtonDisabledColor(colorKey: string): string {
  switch (colorKey) {
    case 'primary':
      return 'primary_lighter';
    default:
      return colorKey;
  }
}

export function getButtonBorderColor(colorKey: string): string {
  switch (colorKey) {
    case 'negative_light':
      return 'negative_light';
    case 'light':
      return 'primary';
    case 'primary_lighter':
      return 'primary_light';
    case 'primary':
      return 'primary_dark';
    default:
      return colorKey;
  }
}

export default theme;
