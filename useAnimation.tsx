import { useEffect, useState } from 'react';
import { Animated } from 'react-native';

type useAnimationType = {
  delay?: number;
  duration?: number;
  inputColor?: object;
  outputColor?: object;
  outputRange?: number;
  type: string;
};

function useAnimation({
  delay,
  duration,
  inputColor,
  outputColor,
  outputRange,
  type }: useAnimationType): any {
  const [animatedValue] = useState(new Animated.Value(0));
  const [animation, setAnimation] = useState(() => { });
  const [animationStyle, setAnimationStyle] = useState({});

  useEffect(() => {
    animatedValue.setValue(0);
  });

  function slideOutDownAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      transform: [
        {
          translateY: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, outputRange]
          })
        }
      ]
    });
  }

  function slideInLeftAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      transform: [
        {
          translateX: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-outputRange, 0]
          })
        }
      ]
    });
  }

  function slideOutUpAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      transform: [
        {
          translateY: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -outputRange]
          })
        }
      ]
    });
  }

  function slideInUpAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      transform: [
        {
          translateY: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [outputRange, 0]
          })
        }
      ]
    });
  }

  function slideInRightAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      transform: [
        {
          translateX: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [outputRange, 0]
          })
        }
      ]
    });
  }

  function fadeOutAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      opacity: animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0]
      })
    });
  }

  function fadeInAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1,
      useNativeDriver: true
    }));
    setAnimationStyle({
      opacity: animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1]
      })
    });
  }

  function backgroundColorAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1
    }));
    setAnimationStyle({
      backgroundColor: animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [inputColor, outputColor]
      })
    });
  }

  function borderColorAnimation(): void {
    setAnimation(() => Animated.timing(animatedValue, {
      duration,
      toValue: 1
    }));
    setAnimationStyle({
      borderColor: animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [inputColor, outputColor]
      })
    });
  }

  function slideInOutAnimation(): void {
    setAnimation(() => Animated.sequence([
      Animated.timing(animatedValue, {
        duration,
        toValue: 1,
        useNativeDriver: true
      }),
      Animated.timing(animatedValue, {
        delay,
        duration,
        toValue: 2,
        useNativeDriver: true
      })
    ]));
    setAnimationStyle({
      transform: [
        {
          translateY: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [outputRange, 0]
          })
        }
      ]
    });
  }

  function fadeInOutAnimation(): void {
    setAnimation(() => Animated.sequence([
      Animated.timing(animatedValue, {
        duration,
        toValue: 1,
        useNativeDriver: true
      }),
      Animated.timing(animatedValue, {
        delay,
        toValue: 0,
        duration,
        useNativeDriver: true
      })
    ]));
    setAnimationStyle({
      opacity: animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1]
      })
    });
  }

  useEffect(() => {
    switch (type) {
      case 'backgroundColorAnimate':
        backgroundColorAnimation();
        break;
      case 'borderColorAnimate':
        borderColorAnimation();
        break;
      case 'fadeIn':
        fadeInAnimation();
        break;
      case 'fadeInOut':
        fadeInOutAnimation();
        break;
      case 'fadeOut':
        fadeOutAnimation();
        break;
      case 'slideInLeft':
        slideInLeftAnimation();
        break;
      case 'slideInRight':
        slideInRightAnimation();
        break;
      case 'slideInUp':
        slideInUpAnimation();
        break;
      case 'slideInOut':
        slideInOutAnimation();
        break;
      case 'slideOutDown':
        slideOutDownAnimation();
        break;
      case 'slideOutUp':
        slideOutUpAnimation();
        break;
      default:
        break;
    }
  }, [type]);

  return [animation, animationStyle];
}

export default useAnimation;
